import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxFooterComponent } from './ngx-footer.component';

describe('footer', () => {
  let component: footer;
  let fixture: ComponentFixture<footer>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ footer ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(footer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
