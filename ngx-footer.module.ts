import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxMaterialModule } from '@4geit/ngx-material-module';
import { NgxFooterComponent } from './ngx-footer.component';

@NgModule({
  imports: [
    CommonModule,
    NgxMaterialModule
  ],
  declarations: [
    NgxFooterComponent
  ],
  exports: [
    NgxFooterComponent
  ]
})
export class NgxFooterModule { }
