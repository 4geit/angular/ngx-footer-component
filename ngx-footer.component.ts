import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  template: require('pug-loader!./ngx-footer.component.pug')(),
  styleUrls: ['./ngx-footer.component.scss']
})
export class NgxFooterComponent implements OnInit {

  @Input() text = '';

  constructor() { }

  ngOnInit() {
  }

}
